# Introduction

Ce script parcourt toute l'arborsence du [« Doc en stock SGDF »](https://www.sgdf.fr/vos-ressources/doc-en-stock) pour générer un fichier html: `index.html`.
Tous les fichiers sont ainsi visibles sur une seule page et triés par date. Il est donc plus facile de retrouver un document.

« Doc en stock » affiche une date pour chacun des documents. Malheureusement, elle est parfois manquante ou fausse. C'est pourquoi, ce script télécharge tous les fichiers pour pouvoir lire leurs métadonnées et récupérer la bonne date. Lorsque cela n'est pas possible, la date de « Doc en stock » est affichée. En cas de date manquante, le fichier est affiché à la fin dans la rubrique « documents sans date ».

Lorsque le fichier est trop volumineux (principalement des vidéos), le fichier n'est pas téléchargé. Si l'url est enregistrée dans la variable `BLACKLIST` de `utils.py`, cette date est utilisée. Sinon, la date fournie par « Doc en stock » est retournée.

# Installation

Il est recommandé d'utiliser un environnement virtuel :

```sh
$ python3 -m venv venv
$ source venv/bin/activate
$ pip3 install -r requirements.txt
```

# Utilisation

Le fichier `index.html` est généré à partir des résultats du scripts et du fichier `template.html`. Le fichier `main.js` gère la barre de recherche.

```sh
$ python3 scraper.py
```
Un mode débogage est disponible :

```sh
$ python3 scraper.py -d
```


--------------------------------------------------------------------------------

This Program is licensed under the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or (at
your option) any later version.
