# Copyright 2019 Jean Felder
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with GNOME Music; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

from datetime import datetime as dt
import io
import logging
import lxml
import os
import subprocess
from urllib.parse import urlparse

from PIL import Image
from PIL.ExifTags import TAGS
from PyPDF2 import PdfFileReader
from PyPDF2.utils import PdfReadError
from rarfile import RarFile
import requests
from zipfile import ZipFile


OFFICE2007_FORMATS = [
    'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
    'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
    'application/vnd.openxmlformats-officedocument.presentationml.presentation'
    ]

OFFICEOLD_FORMATS = [
    'application/vnd.ms-excel',
    'application/msword',
    'application/powerpoint'
    ]

FORBIDDEN_TYPES = ['video/mp4']
MAX_SIZE = 25 * 1024 * 1024


BLACKLIST = {
    'https://www.sgdf.fr/images/stories/LancementPOV308102015.mp4':  '07/10/2015',  # noqa: E501
    'https://www.sgdf.fr/vos-ressources/doc-en-stock/category/180-ag-2014?download=995:video-de-retour-sur-cap-2025': '02/10/2014',  # noqa: E501
    'https://www.sgdf.fr/vos-ressources/doc-en-stock/category/222-videos?download=1630:l-aftermovie-des-camps-2016': '22/09/2016',  # noqa: E501
    'https://www.sgdf.fr/vos-ressources/doc-en-stock/category/222-videos?download=1628:les-medias-parlent-du-scoutisme-en-quartier':  '10/10/2016',  # noqa: E501
    'https://www.sgdf.fr/vos-ressources/doc-en-stock/category/222-videos?download=1627:eduquer-a-la-paix-dans-le-scoutisme':  '08/09/2016',  # noqa: E501
    'https://www.sgdf.fr/vos-ressources/doc-en-stock/category/222-videos?download=1626:video-temoignages-de-chefs-et-cheftaines': '29/09/2016',  # noqa: E501
    'https://www.sgdf.fr/vos-ressources/doc-en-stock/category/222-videos?download=1507:video-de-presentation-des-compagnons': '18/05/2016',  # noqa: E501
    'https://www.sgdf.fr/vos-ressources/doc-en-stock/category/222-videos?download=1100:clip-de-presentation-des-scouts-et-guides-de-france': '20/05/2016',  # noqa: E501
    'https://www.sgdf.fr/vos-ressources/doc-en-stock/category/222-videos?download=1506:clip-de-recrutement-de-chefs':  '18/05/2016',  # noqa: E501
    'https://www.sgdf.fr/vos-ressources/doc-en-stock/category/222-videos?download=887:clips-de-branches-a-telecharger': '18/05/2016',  # noqa: E501
    'https://www.sgdf.fr/vos-ressources/doc-en-stock/category/177-agora?download=1747:agora-2016-video-l-agora-vue-par-les-representants': '08/02/2017',  # noqa: E501
    'https://www.sgdf.fr/vos-ressources/doc-en-stock/category/177-agora?download=1746:agora-2016-video-aftermovie': '08/02/2017',  # noqa: E501
    'https://www.sgdf.fr/vos-ressources/doc-en-stock/category/177-agora?download=1742:agora-2016-video-de-presentation-de-la-thematique': '19/10/2016',  # noqa: E501
    'https://www.sgdf.fr/vos-ressources/doc-en-stock/category/177-agora?download=1738:agora-2016-video-de-presentation': '18/09/2016',  # noqa: E501
    'https://www.sgdf.fr/vos-ressources/doc-en-stock/category/188-formations?download=1434:fc1-echanges-de-pratiques-spi': '07/03/2016',  # noqa: E501
    'https://www.sgdf.fr/vos-ressources/doc-en-stock/category/188-formations?download=1427:formations-complementaires-video-fh02': '04/03/2016',  # noqa: E501
    'https://www.sgdf.fr/vos-ressources/doc-en-stock/category/188-formations?download=1426:formations-complementaires-video-fh01': '04/03/2016',  # noqa: E501
    'https://www.sgdf.fr/vos-ressources/doc-en-stock/category/165-louveteaux-jeannettes?download=1804:gps-louveteaux-jeannettes': '06/09/2013',  # noqa: E501
    'https://www.sgdf.fr/vos-ressources/doc-en-stock/category/168-communication?download=1229:clip-de-lancement': '03/06/2013',  # noqa: E501
    'https://www.sgdf.fr/vos-ressources/doc-en-stock/category/174-demarche-spirituelle?download=1264:video-priere-des-louveteaux-jeannettes': '29/12/2013',  # noqa: E501
}


def clean_url(url):
    """Remove leading and trailing whitespaces from url.

    :param str url: an url
    :returns: cleaned url
    :rtype: str
    """

    return url.strip()


def get_file_extension(headers, url):
    """Get the file extension based on headers or url.

    :param dict headers: request headers
    :param str url: url
    :returns: an extension (pdf, doc, ...)
    :rtype: str
    """
    try:
        attachment = headers['Content-Disposition']
        filename = attachment.split(';')[1]
        filename = filename.split('=')[1].replace('"', '')
    except KeyError:
        try:
            location = urlparse(headers['Location'])
            filename = location.path
        except KeyError:
            filename = urlparse(url).path

    name, extension = os.path.splitext(filename)
    return extension[1:]


def get_infos(url, parent_url, display_date):
    try:
        head = requests.head(url)
        if (head.status_code == 404
           or (head.status_code == 303
               and clean_url(head.headers['Location']) == parent_url)):
            return None, None

        headers = head.headers
        if (('Content-Type' in headers
                and headers['Content-Type'] in FORBIDDEN_TYPES)
            or ('Content-Length' in headers
                and int(headers['Content-Length']) > MAX_SIZE)):
            logger = logging.getLogger()
            logger.debug(
                "cannot download file from url {}. Reason: too big.".format(
                    url))
            try:
                date = dt.strptime(BLACKLIST[url], '%d/%m/%Y')
            except KeyError:
                logger.debug(
                    "url {} is missing from the blacklist".format(url))
                date = display_date

            ext = get_file_extension(headers, url)
            return date, ext

        ext = get_file_extension(headers, url)
        response = requests.get(url)
    except requests.exceptions.MissingSchema:
        return None, None

    if 'Content-Type' not in response.headers:
        return None, 'ext'
    ctype = response.headers['Content-Type'].strip()
    if 'html' in ctype:
        return display_date, 'html'
    elif 'audio' in ctype:
        return display_date, ext
    elif 'video' in ctype:
        return display_date, ext

    content = io.BytesIO(response.content)
    if ctype in ['application/pdf', 'application/postscript']:
        try:
            pdf = PdfFileReader(content)
        except PdfReadError:
            logger = logging.getLogger()
            logger.error("Unable to open pdf from url {}".format(url))
            return None, ext

        if pdf.isEncrypted:
            pdf.decrypt('')
        pdf_info = pdf.getDocumentInfo()
        if pdf_info is not None:
            try:
                date = pdf_info['/ModDate']
            except KeyError:
                try:
                    date = pdf_info['/CreationDate']
                except KeyError:
                    return display_date, ext
            try:
                if date[1] == ':':
                    date = dt.strptime(date[2:10], '%Y%m%d')
                else:
                    date = dt.strptime(date[:8], '%Y%m%d')
            except ValueError:
                date = dt.strptime(date[6:], '%d/%m/%Y')
            return date, ext
        else:
            return display_date, ext
    elif ctype == 'application/x-zip':
        with ZipFile(content, 'r') as zf:
            mod_date = dt.min
            for info in zf.infolist():
                date = dt(*info.date_time)
                if date > mod_date:
                    mod_date = date
        return mod_date, ext
    elif ctype == 'application/x-rar-compressed':
        with RarFile(content) as rf:
            mod_date = dt.min
            for info in rf.infolist():
                mtime = info.mtime
                if info.extract_version == 50:
                    mtime = mtime.replace(tzinfo=None)
                if mtime > mod_date:
                    mod_date = mtime
        return mod_date, ext
    elif 'image' in ctype:
        # test xmp metadata
        content = response.content
        xmp_start = content.find(b'<xmp:MetadataDate>')
        xmp_end = content.find(b'</xmp:MetadataDate>')
        if xmp_start != -1 and xmp_end != -1:
            xmp_bytes = content[xmp_start+len(b'<xmp:MetadataDate>'):xmp_end]
            date = dt.strptime(xmp_bytes[:10].decode('utf-8'), "%Y-%m-%d")
            return date, ext
        else:
            # test tags info
            try:
                i = Image.open(io.BytesIO(content))
                for tag, value in i._getexif().items():
                    decoded = TAGS.get(tag, tag)
                    if decoded == 'DateTime':
                        date = dt.strptime(value[:10], "%Y:%m:%d")
                        return date, ext
                return display_date, ext
            except AttributeError:
                return display_date, ext
    elif ctype in OFFICE2007_FORMATS:
        with ZipFile(content, 'r') as zf:
            doc = lxml.etree.fromstring(zf.read('docProps/core.xml'))
            ns = {'dcterms': 'http://purl.org/dc/terms/'}
            date = doc.xpath('//dcterms:modified', namespaces=ns)
            if not date:
                date = doc.xpath('//dcterms:created', namespaces=ns)
            if date:
                date = date[0].text
                date = dt.strptime(date[:10], "%Y-%m-%d")
                return date, ext
            else:
                return display_date, ext
    elif ctype in OFFICEOLD_FORMATS:
        # very dirty hack...
        with open('/tmp/foo', 'wb') as f:
            f.write(response.content)
        proc = subprocess.Popen(['file', '/tmp/foo'], stdout=subprocess.PIPE)
        tmp = proc.stdout.read()
        date_bytes = b'Last Saved Time/Date:'
        start = tmp.find(date_bytes)
        end = tmp[start:].find(b',') + start
        date = tmp[start+len(date_bytes)+1:end]
        # Tue Jul 18 15:18:59 2017 format
        date = dt.strptime(date.decode('utf-8'), '%a %b %d %H:%M:%S %Y')
        return date, ext
    else:
        return None, ext
