FROM nginx:latest
COPY Pipfile Pipfile.lock /scraper/

RUN set -ex && apt-get update && apt-get install -y python3 pipenv cron
RUN set -ex && cd /scraper && pipenv install --deploy --system

COPY . /scraper/

RUN set -ex && chmod +x /scraper/docker-entrypoint.sh /scraper/scraper.py
VOLUME /usr/share/nginx/html
CMD ["nginx", "-g", "daemon off;"]
ENTRYPOINT ["/scraper/docker-entrypoint.sh"]
